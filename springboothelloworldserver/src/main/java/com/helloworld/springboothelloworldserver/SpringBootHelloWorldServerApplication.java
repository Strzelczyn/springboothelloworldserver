package com.helloworld.springboothelloworldserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHelloWorldServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHelloWorldServerApplication.class, args);
	}

}
