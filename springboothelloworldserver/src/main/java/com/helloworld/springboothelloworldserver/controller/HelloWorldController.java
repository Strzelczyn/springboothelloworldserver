package com.helloworld.springboothelloworldserver.controller;


import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class HelloWorldController {

    @GetMapping(value = "/gets")
    public String contact() {
        return "Hello World";
    }

    @PostMapping(value = "/posts")
    public String updateContact(@RequestBody String text) {
        String answer = "Message: " + text;
        System.out.println(answer);
        return answer;
    }
}
